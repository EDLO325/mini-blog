$(function ()  {
   var APPLICATION_ID = "24A1C394-0DC6-1B55-FF4F-E7A27CF61600",
       SECRET_KEY = "BFC8D50D-A695-A5ED-FF28-2150F030B500",
       VERSION = "v1";
       
       Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
       
       var postscollection = Backendless.Persistence.of(Posts).find();
       
       console.log(postscollection);
       
       var wrapper = {
           posts: postscollection.data
       };
       
       Handlebars.registerHelper('format', function (time){
           return moment(time).format("dddd, MMMM Do YYYY");
       });
       
       var blogScript = $("#blogs-template").html();
       var blogTemplate = Handlebars.compile(blogScript);
       var blogHTML = blogTemplate(wrapper);
       
       $('.main-container').html(blogHTML);
       

});

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}